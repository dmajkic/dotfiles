#!/bin/bash

pushd $(dirname "${0}") > /dev/null
SCRIPTPATH=$(pwd -L)
popd > /dev/null

ln -s $SCRIPTPATH/gemrc ~/.gemrc
ln -s $SCRIPTPATH/gitconfig ~/.gitconfig
ln -s $SCRIPTPATH/vimrc.before ~/.vimrc.before
ln -s $SCRIPTPATH/vimrc.after ~/.vimrc.after
ln -s $SCRIPTPATH/tmux.conf ~/.tmux.conf


